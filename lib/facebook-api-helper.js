var nodeRequest = require("request-promise-native");

class FacebookAPIHelper {
    constructor(token, options = {}) {
        this.token = token;
        this.config = {
            protocol: options.protocol || "https",
            version: options.version || "v2.12"
        }
    }

    static get URL() {
        return {
            ME_URL: "me",
            SEARCH_URL: "search", //place&center=37.484531,-122.148061&fields=id
            PLACE_EVENTS_URL: "/events"
        }
    }

    get accessToken() {
        return this.token;
    }

    request(api, queryParams = {}, dataField) {
        if (!queryParams['access_token']) {
            queryParams['access_token'] = this.accessToken;
        }
        if (!queryParams['limit']) {
            queryParams['limit'] = 25;
        }
        const url = {
            url: `${this.config.protocol}://graph.facebook.com/${this.config.version}/${api}`,
            qs: queryParams
        };

        return this.paginateRequest(url, dataField);
    }

    paginateRequest(url, dataField, data) {
        if (!data) {
            data = [];
        }
        return nodeRequest(url).then(response => {
            response = JSON.parse(response);
            if (dataField && response[dataField] && response[dataField]["data"]) {
                response = response[dataField];
            }

            if (response && response.paging && response.paging.next) {
                return this.paginateRequest(response.paging.next, dataField, data.concat(response.data));
            }
            else if (response && response.data) {
                return {
                    data: data.concat(response.data)
                };
            }
            else {
                return response; //{ data: data.concat(response) };
            }
        });
    }
}

module.exports = FacebookAPIHelper;

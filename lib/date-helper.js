class DateHelper {
    constructor(date) {
        if (!date) {
            this.date = new Date();
        }
    }
    updateDay(total) {
        this.date.setDate(this.date.getDate() + total);
    }
    toFacebookFormat() {
        return (this.date.getTime() / 1000).toFixed();
    }
}

module.exports = DateHelper;

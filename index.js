var q = require("q");
var FacebookAPIHelper = require("./lib/facebook-api-helper");
var DateHelper = require("./lib/date-helper");

const TEST_TOKEN = "EAACEdEose0cBAFZAlJ3XWLmXZBX0DDARZCwtFsqqAEYl13n1WOqEEZAzVev4ZAZBwnsZAZCHIZAykj0T8EhQAlfo2FmTg2jkinrYZCuPNrMH9WsK7QDGk8TAcI4FAQWIa6y8SSTfOy5vEl19Qf1zq7rzGYSRWnZCsrxolMH8NjcYss7vX6tLjZBvVh7u8ZCL05s3ff0wZD";

class FacebookEventsAPI {
    constructor(token, options = {}) {
        this._token = token;
        this._options = options;
        this._fbAPI = new FacebookAPIHelper(this._token);
        this._startDate = new DateHelper();
        this._endDate = new DateHelper();
    }
    setStartDateFromToday(howMany) {
        this._startDate.updateDay(howMany);
    }
    setEndDateFromToday(howMany) {
        this._endDate.updateDay(howMany);
    }
    getEvents() {
        return this._fbAPI.request(FacebookAPIHelper.URL.SEARCH_URL, { type: "place", center: "-30.0573146,-51.171436", distance: "10000", fields: "id,name" }).then(placesResponse => {
            let places = placesResponse.data;
            let promises = [];
            places.forEach(place => {
                promises.push(this._fbAPI.request(place.id + FacebookAPIHelper.URL.PLACE_EVENTS_URL, { since: this._startDate.toFacebookFormat(), until: this._endDate.toFacebookFormat() }));
            });
            console.log("Places found: ", places.length);
            return q.all(promises);
        }).then(placesEventsResponse => {
            let events = [];
            let placesWithEvents = placesEventsResponse.filter(placeEventsResponse => { return placeEventsResponse.data });
            placesWithEvents.forEach(placeEvents => {
                events = events.concat(placeEvents.data);
            });
            console.log("Total events: ", events.length);
            events.forEach(event => {
                console.log(event.id, event.name, event.start_time);
            });
            return events;
        });
    }
}

module.exports = FacebookEventsAPI;

//FB EVENTS API EXAMPLES
// let fbAPI = new FacebookEventsAPI(TEST_TOKEN);
// fbAPI.setStartDateFromToday(-1);
// fbAPI.setEndDateFromToday(+2);
// fbAPI.getEvents().then(events => {
//     console.log(events);
// });


//FB GRAPH API EXAMPLES

//get places close
//search?type=place&center=-30.0563488,-51.1742254&distance=1000&fields=events&since=1504188000

//get events of place
//286015814865919/events?fields=id,name,location&filtering=[{"field":"start_time","operator":"BEFORE","value":"2017-12-20T20:00:00-0300"}]

//params to filter by event time
//since,until!!!
